# booklon

This template should help get you started developing with Vue 3 in Vite.

## Pages
* Page d'accueil : affiche le planning du jour pour la fabrique sélectionnée
* Page choix fabrique : affiche la liste des fabriques par région et permet de sélectionner sa fabrique
* Page Manage room : affiche le crud des salles de la fabrique sélectionnée
* Page Manage promo : affiche le crud des promos de la fabrique sélectionnée
* Page Manage fabrique : affiche le crud des fabriques et des régions
* Page planning : affiche les booking des salle et leur promo assignée, sur une période donnée (jour, semaine, mois), pour la fabrique sélectionnée. Vue par promo/vue par salle
* Modal booking : au click sur une salle, depuis un peu n'importe où, formulaire avec date début et date de fin et choix de promo pour réserver une salle