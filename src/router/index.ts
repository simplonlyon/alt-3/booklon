import { useAuthStore } from "@/stores/auth";
import { useFabriqueStore } from "@/stores/fabrique";
import ManageFabrique from "@/views/ManageFabrique.vue";
import ManagePromo from "@/views/ManagePromo.vue";
import SelectFabrique from "@/views/SelectFabrique.vue";
import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";
import Placing from "../views/Placing.vue";
import ManageRooms from "../views/ManageRooms.vue";
import Planning from "../views/Planning.vue";

export const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: HomeView,
    },
    {
      path: "/manage-rooms",
      name: "manage-rooms",
      component: ManageRooms,
    },
    {
      path: "/manage-promos",
      name: "manage-promos",
      component: ManagePromo,
    },
    {
      path: "/manage-fabriques",
      name: "manage-fabriques",
      component: ManageFabrique,
    },
    {
      path: "/select-fabrique",
      name: "fabrique",
      component: SelectFabrique,
    },
    {
      path: '/planning',
      name: 'planning',
      component: Planning
    },
    {
      path: '/placing',
      name: 'placing',
      component: Placing
    }
  ],
});

router.beforeEach((to) => {
  const auth = useAuthStore();
  if(to.name?.toString().includes('manage')) {
    return auth.token != null;
  }
  return true;
});

router.beforeResolve(async (to) => {
  const store = useFabriqueStore();
  const selectFabriquePage = ["/select-fabrique"];
  const fabriqueRequired = !selectFabriquePage.includes(to.path);

  if (fabriqueRequired && !store.selected) { 
    return '/select-fabrique';
  }
});

export default router;
