import axios from 'axios';
import { useAuthStore } from './stores/auth';



axios.interceptors.request.use((config) => {
    const authStore = useAuthStore();
    if(authStore.token && !config.url?.includes('refresh')) {
        config.headers = {
            'Authorization': 'Bearer '+authStore.token
        }
    }
    return config;
});

axios.interceptors.response.use(response => response, async (error) => {
    const authStore = useAuthStore();
    if( !error.config.url?.includes('refresh') && error.response.status == 401 && authStore.refreshToken) {
        const response = await axios.post(import.meta.env.VITE_SERVER_URL+'/api/refresh', {token:authStore.refreshToken});
        authStore.connect(response.data.token, authStore.refreshToken);
        return (<any>axios)[error.config.method](error.config.url, error.config.data?JSON.parse(error.config.data):null);
    } else {
        return Promise.reject(error);
    }

});