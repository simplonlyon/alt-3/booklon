import { ElNotification } from "element-plus";


export async function doAndNotify(entity: any, operation: (entity: any) => Promise<any>, verb: string) {

    try {

        await operation(entity);

        ElNotification({
            title: 'Succès',
            message: `L'opération ${verb} a bien réussie`,
            type: 'success',
        });

    } catch (e) {
        console.error(e);

        ElNotification({
            title: 'Erreur',
            message: `Erreur lors de l\'opération : ${verb}`,
            type: 'error',
        });
    }
}

export async function persistOrUpdate(entity: any, update: (entity: any) => Promise<any>, persist: (entity: any) => Promise<any>) {
    if (entity.id) {
        await doAndNotify(entity, update, 'modifier');
        return;
    }
    await doAndNotify(entity, persist, 'ajouter');
}



export const datesAreOnSameDay = (first: Date, second: Date) =>
    first && second &&
    first.getFullYear() === second.getFullYear() &&
    first.getMonth() === second.getMonth() &&
    first.getDate() === second.getDate();

export const stringToColour = (str: string) => {
    var hash = 0;
    for (var i = 0; i < str.length; i++) {
        hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }
    var colour = '#';
    for (var i = 0; i < 3; i++) {
        var value = (hash >> (i * 8)) & 0xFF;
        colour += ('00' + value.toString(16)).substr(-2);
    }
    return colour;
}