export interface Room {
  id?: string;
  name: string;
  capacity?: number;
  fabrique?: any;
  color?: string;
  promo?: Promo;
}

export interface Booking {
  id?: number;
  startDate: Date;
  endDate: Date;
  room?: Room;
  promo?: Promo;
}

export interface Promo {
  id?: number;
  name: string;
  startDate: Date;
  endDate: Date;
  studentNumber: number;
  fabrique: Fabrique;
  students?:Student[];
}

export interface Student {
  id?:number;
  name:string;
  promo:Promo;
}

export interface Fabrique {
  id?: number;
  name: string;
  region: Region;
}

export interface Region {
  id: number;
  name: string;
}
