import type { Student } from "@/entities";
import { defineStore } from "pinia";
import axios from "axios";
import { useFabriqueStore } from "./fabrique";
import { ref } from "vue";

export const useStudentStore = defineStore("student", () => {

  const students = ref<Student[]>([]);

  function changeStudents(studentsList: Student[]) {
    students.value = studentsList;
  }
  async function persistStudent(student: Student|Student[]) {
    const response = await axios.post(
      import.meta.env.VITE_SERVER_URL + "/api/student",
      student
    );
    if(Array.isArray(response.data)) {
      students.value.push(...response.data);
    } else {
      students.value.push(response.data);
    }
  }
  async function fetchStudents(promoId:number) {
    const response = await axios.get(
      import.meta.env.VITE_SERVER_URL + "/api/student?promo=" + promoId
    );
    students.value = response.data;
  }
  async function fetchRandomized(promoId:number) {
    const response = await axios.get(
      import.meta.env.VITE_SERVER_URL + "/api/student/randomized/" + promoId
    );
    students.value = response.data;
  }
  async function updateStudent(student: Student) {
    await axios.patch(import.meta.env.VITE_SERVER_URL + '/api/student/' + student.id, student);
    const found = students.value.find(item => student.id == item.id);
    if (found) {
      Object.assign(found, student);
    }
  }
  async function removeStudent(student: Student) {
    await axios.delete(import.meta.env.VITE_SERVER_URL + '/api/student/' + student.id);
    students.value = students.value.filter(item => item.id != student.id);
  }
  return { students, changeStudents, persistStudent, fetchStudents, fetchRandomized, updateStudent,removeStudent};
});
