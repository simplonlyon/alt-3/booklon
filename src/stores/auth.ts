import { defineStore } from "pinia";
import { ref } from "vue";


export const useAuthStore = defineStore("auth", () => {

  const token = ref(localStorage.getItem('token'))
  const refreshToken = ref(localStorage.getItem('refreshToken'));

  function connect(tokenP: string, refreshTokenP: string) {
    localStorage.setItem('token', tokenP);
    localStorage.setItem('refreshToken', refreshTokenP);
    token.value = tokenP;
    refreshToken.value = refreshTokenP;
  }

  function logout() {
    token.value = null;
    refreshToken.value = null;
    localStorage.removeItem('token');
    localStorage.removeItem('refreshToken');
  }
  return {token,refreshToken,logout,connect};
});
