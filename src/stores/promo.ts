import type { Promo } from "@/entities";
import { defineStore } from "pinia";
import axios from "axios";
import { useFabriqueStore } from "./fabrique";
import { ref } from "vue";

export const usePromoStore = defineStore("promo", () => {

  const promos = ref<Promo[]>([]);
  const selected = ref<Promo>(localStorage.getItem("selected-promo")
    ? JSON.parse(localStorage.getItem("selected-promo")!)
    : null);


  function changePromos(promosList: Promo[]) {
    promos.value = promosList;
  }
  async function persistPromo(promo: Promo) {
    const response = await axios.post(
      import.meta.env.VITE_SERVER_URL + "/api/promo",
      promo
    );
    fetchPromos();
    promos.value.push(response.data);
  }
  async function fetchPromos() {
    const { selected } = useFabriqueStore();
    const response = await axios.get(
      import.meta.env.VITE_SERVER_URL + "/api/promo?fabrique=" + selected.id
    );
    promos.value = response.data;
  }
  function selectPromo(promo: Promo) {
    selected.value = promo;
    localStorage.setItem("selected-promo", JSON.stringify(promo));
  }

  async function updatePromo(promo: Promo) {
    await axios.patch(import.meta.env.VITE_SERVER_URL + '/api/promo/' + promo.id, promo);
    const found = promos.value.find(item => promo.id == item.id);
    if (found) {
      Object.assign(found, promo);
    }
  }
  async function removePromo(promo: Promo) {
    await axios.delete(import.meta.env.VITE_SERVER_URL + '/api/promo/' + promo.id);
    promos.value = promos.value.filter(item => item.id != promo.id);
  }
  return { promos, selected, changePromos, persistPromo, fetchPromos, selectPromo,updatePromo,removePromo};
});
