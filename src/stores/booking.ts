import type { Booking } from "@/entities";
import axios from "axios";
import { defineStore } from "pinia";
import { ref } from "vue";
import { useFabriqueStore } from "./fabrique";

export const useBookingStore = defineStore("booking", () => {

  const bookings = ref<Booking[]>([]);

  function changeBooking(paramBookings: Booking[]) {
    bookings.value = paramBookings;
  }
  async function persistBookings(booking: Booking) {
    const response = await axios.post(
      import.meta.env.VITE_SERVER_URL + "/api/booking",
      booking
    );
    bookings.value.push(response.data);
  }

  async function updateBooking(booking: Booking) {
    await axios.patch<Booking>(import.meta.env.VITE_SERVER_URL + '/api/booking/' + booking.id, booking);
    
      bookings.value = bookings.value.map(item => {
        if(item.id == booking.id) {
          return booking
        } else {
          return item;
        }
      });
    
  }
  async function removeBooking(booking: Booking) {
    await axios.delete(import.meta.env.VITE_SERVER_URL + '/api/booking/' + booking.id);
    bookings.value = bookings.value.filter(item => item.id != booking.id);
  }
  async function fetchBookings(startDate: Date, endDate: Date) {
    const { selected } = useFabriqueStore();

    const response = await axios.get<Booking[]>(
      import.meta.env.VITE_SERVER_URL +
      `/api/booking/range/${selected.id
      }/${startDate.toISOString().split('T')[0]}/${endDate.toISOString().split('T')[0]}`
    );
    bookings.value = response.data.map((el) => ({ ...el, startDate: new Date(el.startDate), endDate: new Date(el.endDate) }));
  }

  return { bookings, changeBooking, persistBookings, fetchBookings, updateBooking, removeBooking }
});
