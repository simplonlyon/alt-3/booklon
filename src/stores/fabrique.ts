import type { Fabrique, Region } from "@/entities";
import axios from "axios";
import { defineStore } from "pinia";
import { ref } from "vue";

export const useFabriqueStore = defineStore("fabrique", () => {

  const fabriques = ref<Fabrique[]>([]);
  const regions = ref<Region[]>([]);
  const selected = ref<Fabrique>(localStorage.getItem("selected-fabrique")
    ? JSON.parse(localStorage.getItem("selected-fabrique")!)
    : null);

  function changeFabriques(fabriquesList: Fabrique[]) {
    fabriques.value = fabriquesList;
  }
  async function persistFabrique(fabrique: Fabrique) {
    const response = await axios.post(
      import.meta.env.VITE_SERVER_URL + "/api/fabrique",
      fabrique
    );
    fetchFabriques();
  }
  async function fetchFabriques() {
    const response = await axios.get(
      import.meta.env.VITE_SERVER_URL + "/api/fabrique"
    );
    fabriques.value = response.data;
  }
  function selectFabrique(fabrique: Fabrique) {
    selected.value = fabrique;
    localStorage.setItem("selected-fabrique", JSON.stringify(fabrique));
  }
  function changeRegions(regionsList: Region[]) {
    regions.value = regionsList;
  }
  async function persistRegion(region: Region) {
    const response = await axios.post<Region>(
      import.meta.env.VITE_SERVER_URL + "/api/region",
      region
    );
    regions.value.push(response.data);
    return response.data;
  }
  async function fetchRegions() {
    const response = await axios.get(
      import.meta.env.VITE_SERVER_URL + "/api/region"
    );
    regions.value = response.data;
  }

  async function updateFabrique(fabrique: Fabrique) {
    await axios.patch(import.meta.env.VITE_SERVER_URL + '/api/fabrique/' + fabrique.id, fabrique);
    const found = fabriques.value.find(item => fabrique.id == item.id);
    if (found) {
      Object.assign(found, fabrique);
    }
  }
  async function removeFabrique(fabrique: Fabrique) {
    await axios.delete(import.meta.env.VITE_SERVER_URL + '/api/fabrique/' + fabrique.id);
    fabriques.value = fabriques.value.filter(item => item.id != fabrique.id);
  }
  async function removeRegion(region: Region) {
    await axios.delete(import.meta.env.VITE_SERVER_URL + '/api/region/' + region.id);
    regions.value = regions.value.filter(item => item.id != region.id);
  }

  return { fabriques, regions, selected, selectFabrique, fetchFabriques, fetchRegions, changeRegions, changeFabriques, persistRegion, persistFabrique, updateFabrique, removeFabrique, removeRegion };
});
