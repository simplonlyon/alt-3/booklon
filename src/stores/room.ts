import type { Room } from "@/entities";
import axios from "axios";
import { defineStore } from "pinia";
import { ref } from "vue";
import { useFabriqueStore } from "./fabrique";


export const useRoomStore = defineStore('room', () => {

    const rooms = ref<Room[]>([]);

    function changeRooms(roomList: Room[]) {
        rooms.value = roomList;
    }
    async function persistRoom(room: Room) {
        const response = await axios.post(import.meta.env.VITE_SERVER_URL + '/api/room', room);
        rooms.value.push(response.data);
    }
    async function fetchRooms() {
        const { selected } = useFabriqueStore();
        const response = await axios.get(import.meta.env.VITE_SERVER_URL + '/api/room?fabrique=' + selected.id);
        rooms.value = response.data;

    }
    async function updateRoom(room:Room) {
        await axios.patch(import.meta.env.VITE_SERVER_URL + '/api/room/'+room.id, room);
        const found = rooms.value.find(item => room.id == item.id);
        if(found) {
            Object.assign(found, room);
        }
    }
    async function removeRoom(room:Room) {
        await axios.delete(import.meta.env.VITE_SERVER_URL + '/api/room/'+room.id);
        rooms.value = rooms.value.filter(item => item.id != room.id);
    }

    return { rooms, changeRooms, persistRoom, fetchRooms, updateRoom, removeRoom };
});

