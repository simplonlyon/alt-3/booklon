import { createApp } from "vue";
import { createPinia } from "pinia";
import 'dayjs/locale/fr';

import ElementPlus, { dayjs } from "element-plus";
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
import App from "@/App.vue";
import router from "./router";
import './interceptors';

import "./assets/main.scss";

dayjs.locale('fr');

const app = createApp(App);

app.use(ElementPlus);

app.use(createPinia());
app.use(router);

for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
  }

app.mount("#app");
